# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: response.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='response.proto',
  package='bidirectional',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x0eresponse.proto\x12\rbidirectional\"%\n\x14\x43heckResponseRequest\x12\r\n\x05\x41PIId\x18\x01 \x01(\t\"N\n\x0b\x41PIResponse\x12\x17\n\x0fResponseMessage\x18\x01 \x01(\t\x12\x10\n\x08Severity\x18\x02 \x01(\t\x12\x14\n\x0cResponseTime\x18\x03 \x01(\t\"Z\n\tAddNewAPI\x12\x0b\n\x03\x41PI\x18\x01 \x01(\t\x12\x0f\n\x07service\x18\x02 \x01(\t\x12\x17\n\x0ftimeout_warning\x18\x03 \x01(\t\x12\x16\n\x0etimeout_danger\x18\x04 \x01(\t\"I\n\x0e\x45\x64itAPIRequest\x12(\n\x06params\x18\x01 \x01(\x0b\x32\x18.bidirectional.AddNewAPI\x12\r\n\x05\x41PIId\x18\x02 \x01(\t\"!\n\x10\x44\x65leteAPIRequest\x12\r\n\x05\x41PIId\x18\x01 \x01(\t\".\n\tLogInUser\x12\x0f\n\x07\x45mailId\x18\x01 \x01(\t\x12\x10\n\x08PassWord\x18\x02 \x01(\t\"K\n\nSignUpUser\x12\x0c\n\x04Name\x18\x01 \x01(\t\x12\x0f\n\x07\x45mailId\x18\x02 \x01(\t\x12\x10\n\x08PassWord\x18\x03 \x01(\t\x12\x0c\n\x04Role\x18\x04 \x01(\t\"5\n\x08Response\x12\x15\n\rStatusMessage\x18\x01 \x01(\t\x12\x12\n\nStatusCode\x18\x02 \x01(\x05\"\x1a\n\x07Message\x12\x0f\n\x07message\x18\x01 \x01(\t2Z\n\rBidirectional\x12I\n\x11GetServerResponse\x12\x16.bidirectional.Message\x1a\x16.bidirectional.Message\"\x00(\x01\x30\x01\x32M\n\rSignUpService\x12<\n\x06SignUp\x12\x19.bidirectional.SignUpUser\x1a\x17.bidirectional.Response2J\n\x0cLogInService\x12:\n\x05LogIn\x12\x18.bidirectional.LogInUser\x1a\x17.bidirectional.Response2\xa5\x02\n\nAPIService\x12;\n\x06\x41\x64\x64\x41PI\x12\x18.bidirectional.AddNewAPI\x1a\x17.bidirectional.Response\x12\x41\n\x07\x45\x64itAPI\x12\x1d.bidirectional.EditAPIRequest\x1a\x17.bidirectional.Response\x12\x45\n\tDeleteAPI\x12\x1f.bidirectional.DeleteAPIRequest\x1a\x17.bidirectional.Response\x12P\n\rCheckResponse\x12#.bidirectional.CheckResponseRequest\x1a\x1a.bidirectional.APIResponseb\x06proto3'
)




_CHECKRESPONSEREQUEST = _descriptor.Descriptor(
  name='CheckResponseRequest',
  full_name='bidirectional.CheckResponseRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='APIId', full_name='bidirectional.CheckResponseRequest.APIId', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=33,
  serialized_end=70,
)


_APIRESPONSE = _descriptor.Descriptor(
  name='APIResponse',
  full_name='bidirectional.APIResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='ResponseMessage', full_name='bidirectional.APIResponse.ResponseMessage', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Severity', full_name='bidirectional.APIResponse.Severity', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='ResponseTime', full_name='bidirectional.APIResponse.ResponseTime', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=150,
)


_ADDNEWAPI = _descriptor.Descriptor(
  name='AddNewAPI',
  full_name='bidirectional.AddNewAPI',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='API', full_name='bidirectional.AddNewAPI.API', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='service', full_name='bidirectional.AddNewAPI.service', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='timeout_warning', full_name='bidirectional.AddNewAPI.timeout_warning', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='timeout_danger', full_name='bidirectional.AddNewAPI.timeout_danger', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=152,
  serialized_end=242,
)


_EDITAPIREQUEST = _descriptor.Descriptor(
  name='EditAPIRequest',
  full_name='bidirectional.EditAPIRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='params', full_name='bidirectional.EditAPIRequest.params', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='APIId', full_name='bidirectional.EditAPIRequest.APIId', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=244,
  serialized_end=317,
)


_DELETEAPIREQUEST = _descriptor.Descriptor(
  name='DeleteAPIRequest',
  full_name='bidirectional.DeleteAPIRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='APIId', full_name='bidirectional.DeleteAPIRequest.APIId', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=319,
  serialized_end=352,
)


_LOGINUSER = _descriptor.Descriptor(
  name='LogInUser',
  full_name='bidirectional.LogInUser',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='EmailId', full_name='bidirectional.LogInUser.EmailId', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='PassWord', full_name='bidirectional.LogInUser.PassWord', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=354,
  serialized_end=400,
)


_SIGNUPUSER = _descriptor.Descriptor(
  name='SignUpUser',
  full_name='bidirectional.SignUpUser',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Name', full_name='bidirectional.SignUpUser.Name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='EmailId', full_name='bidirectional.SignUpUser.EmailId', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='PassWord', full_name='bidirectional.SignUpUser.PassWord', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Role', full_name='bidirectional.SignUpUser.Role', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=402,
  serialized_end=477,
)


_RESPONSE = _descriptor.Descriptor(
  name='Response',
  full_name='bidirectional.Response',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='StatusMessage', full_name='bidirectional.Response.StatusMessage', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='StatusCode', full_name='bidirectional.Response.StatusCode', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=479,
  serialized_end=532,
)


_MESSAGE = _descriptor.Descriptor(
  name='Message',
  full_name='bidirectional.Message',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='message', full_name='bidirectional.Message.message', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=534,
  serialized_end=560,
)

_EDITAPIREQUEST.fields_by_name['params'].message_type = _ADDNEWAPI
DESCRIPTOR.message_types_by_name['CheckResponseRequest'] = _CHECKRESPONSEREQUEST
DESCRIPTOR.message_types_by_name['APIResponse'] = _APIRESPONSE
DESCRIPTOR.message_types_by_name['AddNewAPI'] = _ADDNEWAPI
DESCRIPTOR.message_types_by_name['EditAPIRequest'] = _EDITAPIREQUEST
DESCRIPTOR.message_types_by_name['DeleteAPIRequest'] = _DELETEAPIREQUEST
DESCRIPTOR.message_types_by_name['LogInUser'] = _LOGINUSER
DESCRIPTOR.message_types_by_name['SignUpUser'] = _SIGNUPUSER
DESCRIPTOR.message_types_by_name['Response'] = _RESPONSE
DESCRIPTOR.message_types_by_name['Message'] = _MESSAGE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

CheckResponseRequest = _reflection.GeneratedProtocolMessageType('CheckResponseRequest', (_message.Message,), {
  'DESCRIPTOR' : _CHECKRESPONSEREQUEST,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.CheckResponseRequest)
  })
_sym_db.RegisterMessage(CheckResponseRequest)

APIResponse = _reflection.GeneratedProtocolMessageType('APIResponse', (_message.Message,), {
  'DESCRIPTOR' : _APIRESPONSE,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.APIResponse)
  })
_sym_db.RegisterMessage(APIResponse)

AddNewAPI = _reflection.GeneratedProtocolMessageType('AddNewAPI', (_message.Message,), {
  'DESCRIPTOR' : _ADDNEWAPI,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.AddNewAPI)
  })
_sym_db.RegisterMessage(AddNewAPI)

EditAPIRequest = _reflection.GeneratedProtocolMessageType('EditAPIRequest', (_message.Message,), {
  'DESCRIPTOR' : _EDITAPIREQUEST,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.EditAPIRequest)
  })
_sym_db.RegisterMessage(EditAPIRequest)

DeleteAPIRequest = _reflection.GeneratedProtocolMessageType('DeleteAPIRequest', (_message.Message,), {
  'DESCRIPTOR' : _DELETEAPIREQUEST,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.DeleteAPIRequest)
  })
_sym_db.RegisterMessage(DeleteAPIRequest)

LogInUser = _reflection.GeneratedProtocolMessageType('LogInUser', (_message.Message,), {
  'DESCRIPTOR' : _LOGINUSER,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.LogInUser)
  })
_sym_db.RegisterMessage(LogInUser)

SignUpUser = _reflection.GeneratedProtocolMessageType('SignUpUser', (_message.Message,), {
  'DESCRIPTOR' : _SIGNUPUSER,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.SignUpUser)
  })
_sym_db.RegisterMessage(SignUpUser)

Response = _reflection.GeneratedProtocolMessageType('Response', (_message.Message,), {
  'DESCRIPTOR' : _RESPONSE,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.Response)
  })
_sym_db.RegisterMessage(Response)

Message = _reflection.GeneratedProtocolMessageType('Message', (_message.Message,), {
  'DESCRIPTOR' : _MESSAGE,
  '__module__' : 'response_pb2'
  # @@protoc_insertion_point(class_scope:bidirectional.Message)
  })
_sym_db.RegisterMessage(Message)



_BIDIRECTIONAL = _descriptor.ServiceDescriptor(
  name='Bidirectional',
  full_name='bidirectional.Bidirectional',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=562,
  serialized_end=652,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetServerResponse',
    full_name='bidirectional.Bidirectional.GetServerResponse',
    index=0,
    containing_service=None,
    input_type=_MESSAGE,
    output_type=_MESSAGE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_BIDIRECTIONAL)

DESCRIPTOR.services_by_name['Bidirectional'] = _BIDIRECTIONAL


_SIGNUPSERVICE = _descriptor.ServiceDescriptor(
  name='SignUpService',
  full_name='bidirectional.SignUpService',
  file=DESCRIPTOR,
  index=1,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=654,
  serialized_end=731,
  methods=[
  _descriptor.MethodDescriptor(
    name='SignUp',
    full_name='bidirectional.SignUpService.SignUp',
    index=0,
    containing_service=None,
    input_type=_SIGNUPUSER,
    output_type=_RESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_SIGNUPSERVICE)

DESCRIPTOR.services_by_name['SignUpService'] = _SIGNUPSERVICE


_LOGINSERVICE = _descriptor.ServiceDescriptor(
  name='LogInService',
  full_name='bidirectional.LogInService',
  file=DESCRIPTOR,
  index=2,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=733,
  serialized_end=807,
  methods=[
  _descriptor.MethodDescriptor(
    name='LogIn',
    full_name='bidirectional.LogInService.LogIn',
    index=0,
    containing_service=None,
    input_type=_LOGINUSER,
    output_type=_RESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_LOGINSERVICE)

DESCRIPTOR.services_by_name['LogInService'] = _LOGINSERVICE


_APISERVICE = _descriptor.ServiceDescriptor(
  name='APIService',
  full_name='bidirectional.APIService',
  file=DESCRIPTOR,
  index=3,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=810,
  serialized_end=1103,
  methods=[
  _descriptor.MethodDescriptor(
    name='AddAPI',
    full_name='bidirectional.APIService.AddAPI',
    index=0,
    containing_service=None,
    input_type=_ADDNEWAPI,
    output_type=_RESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='EditAPI',
    full_name='bidirectional.APIService.EditAPI',
    index=1,
    containing_service=None,
    input_type=_EDITAPIREQUEST,
    output_type=_RESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='DeleteAPI',
    full_name='bidirectional.APIService.DeleteAPI',
    index=2,
    containing_service=None,
    input_type=_DELETEAPIREQUEST,
    output_type=_RESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='CheckResponse',
    full_name='bidirectional.APIService.CheckResponse',
    index=3,
    containing_service=None,
    input_type=_CHECKRESPONSEREQUEST,
    output_type=_APIRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_APISERVICE)

DESCRIPTOR.services_by_name['APIService'] = _APISERVICE

# @@protoc_insertion_point(module_scope)
