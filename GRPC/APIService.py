import response_pb2
import response_pb2_grpc
import configparser
from utils import Utils

class APIService(response_pb2_grpc.APIServiceServicer):
    
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read(".env")
        self.utils = Utils()
        
    """
    To add a new API
    """
    def AddAPI(self,request,context):
        
        try:
            API = request.API
            service = request.service
            timeout_warning = request.timeout_warning
            timeout_danger = request.timeout_danger
            print(API,service,timeout_warning,timeout_danger)
            print(API,service)
            status,message,code = self.utils.checkInput([API,service,timeout_warning,timeout_danger])
            # print(status,message,code)
            if status == False:
                return response_pb2.Response(StatusMessage=message,StatusCode=int(code))
            status,message,code = self.utils.addNewAPI(API,service,timeout_warning,timeout_danger)
            # print(status,message,code)
            if status == False:
                return response_pb2.Response(StatusMessage=message,StatusCode=int(code)) 
            return response_pb2.Response(StatusMessage=message,StatusCode=int(code))
        except Exception as e:
            print("exception",e)
            return response_pb2.Response(StatusMessage=e,StatusCode=500)
    
    """
    To Edit an existing API
    """                                     
    def EditAPI(self,request,context):
        try:
            API = request.params.API
            service = request.params.service
            APIId = request.APIId
            # print(API,service,APIId)
            timeout_warning = request.params.timeout_warning
            timeout_danger = request.params.timeout_danger
            # print(API,service,timeout_warning,timeout_danger)
            status,message,code = self.utils.EditAPI(API,service,APIId,timeout_warning,timeout_danger)
            print(status,message,code)
            if status == False:
                return response_pb2.Response(StatusMessage=message,StatusCode=int(code))
            return response_pb2.Response(StatusMessage=message,StatusCode=int(code))
        except Exception as e:
            print(e)
            return response_pb2.Response(StatusMessage=e,StatusCode=int(code))
    
    """
    To Delete an existing API
    Actually data is not deleted, it is deactivated
    """
    def DeleteAPI(self,request,context):
        try:
            APIId = request.APIId
            status,message,code = self.utils.DeleteAPI(APIId)
            print(status,message,code)
            if status == False:
                return response_pb2.Response(StatusMessage=message,StatusCode=int(code))
            return response_pb2.Response(StatusMessage=message,StatusCode=int(code))
        except Exception as e:
            print(e)
            return response_pb2.Response(StatusMessage=e,StatusCode=int(code))
        
        
    """
    To check the response of an API
    """
    def CheckResponse(self,request,context):
        try:
            APIId = request.APIId
            status,message,code = self.utils.CheckAPI(APIId)
            print(status,message,code)
            if status == False:
                return response_pb2.APIResponse(ResponseMessage=message,
                                             Severity="Not Applicable",
                                             ResponseTime="Not Applicable")
            return response_pb2.APIResponse(ResponseMessage=message,
                                             Severity="Not Applicable",
                                             ResponseTime="Not Applicable")
        except Exception as e:
            print(e)
            return response_pb2.APIResponse(ResponseMessage=e,
                                             Severity="Not Applicable",
                                             ResponseTime="Not Applicable")
        