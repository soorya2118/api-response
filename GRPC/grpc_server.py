from concurrent import futures
import grpc
import response_pb2
import response_pb2_grpc
import configparser
from test import BidirectionalService
import time
from APIService import APIService

    
def serve():
    config = configparser.ConfigParser()
    config.read('.env')
    workers = int(config['grpc']['workers'])
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=workers))
    response_pb2_grpc.add_BidirectionalServicer_to_server(BidirectionalService(), server)
    response_pb2_grpc.add_APIServiceServicer_to_server(APIService(),server)
    server.add_insecure_port('0.0.0.0:50051')
    server.start()
    try:
        while True:
            time.sleep(86400) # one day in seconds
    except KeyboardInterrupt:
        print("Exiting.. bye!")
        server.stop(0)

if __name__ == "__main__":
    serve()