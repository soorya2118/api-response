import configparser
import response_pb2_grpc
import response_pb2
from pymongo import MongoClient
from bson.objectid import ObjectId
import requests



class Utils:
    
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read(".env")
        ConnectionString = self.config['mongo_atlas']['url']
        try:
            connection =MongoClient(ConnectionString)
            # print("Connected to DB")
            self.db=connection.APIResponse
            self.APIs = connection.APIResponse['APIdetails']
        except Exception as e:
            print(e) 

    def checkInput(self,input_list):
        try:
            for item in input_list:
                if (item == "None") or (item =="") or (item == "none"):
                    return False,"One of the inputs is empty","400"
                return True,"Input not Empty","200"
        except Exception as e:
            print(e)
            return False,e,"500"
    def addNewAPI(self,API,service,timeout_warning,timeout_danger):
        try:
            self.APIs.insert_one({
                'api'       : API,
                'service'   : service,
                'timeout_warning' : timeout_warning,
                'timeout_danger'  : timeout_danger,
                'isDeleted' : False
                
            })
            return True,"New API Inserted","200"
        except Exception as e:
            print(e)
            return False,e,"500"
    def EditAPI(self,API,service,APIId,timeout_warning,timeout_danger):
        try:
            doc = {
                'api'       : API,
                'service'   : service,
                'timeout_warning' : timeout_warning,
                'timeout_danger'  : timeout_danger,
                'isDeleted' : False
            }
            
            result = self.APIs.update_one(
                {
                "_id":ObjectId(APIId),
                "isDeleted":False
                },{'$set': doc})
            if result.modified_count == 0:
                return False,"No such API found to edit","400"
            return True,"Updation success","200"
        except Exception as e:
            return False,e,"500"
    def DeleteAPI(self,APIId):
        try:
            result = self.APIs.update_one({"_id":ObjectId(APIId)},{'$set': {'isDeleted':True}})
            if result.modified_count == 0:
                return False,"No such item found","400"
            return True,"Deletion success","200"
        except Exception as e:
            print(e)
            return False,e,"500"
    
    def CheckAPI(self,APIId):
        try:
            # print("inside check api")
            result = list(self.APIs.find({"_id":ObjectId(APIId)}))
            
            warning_time = result[0]['timeout_warning']
            danger_time = result[0]['timeout_danger']
            
            if len(result) == 0:
                return False,"No such item found","400"
            response=requests.get(result[0]['api'])
           
            if response.status_code == 200:
                response_time = response.elapsed
                response_time = (str(response_time).split(":"))
                
                response_time = ((float(response_time[2])))
                if response_time >= int(warning_time):
                    return True,"warning",response.status_code
                if response_time >= int(danger_time):
                    return True,"danger",response.status_code
                return True,"OK",response.status_code
            return False,"Error in getting response",response.status_code
        except Exception as e:
            print(e)
            return False,e,"500"
        
            
            
            
        
# ob= Utils()
# res,msg,code = ob.checkInput([1,2])
# print(res,msg,code)